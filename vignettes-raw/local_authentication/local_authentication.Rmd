---
title: "Local authentication"
date: "`r format(Sys.Date(), '%B %-d, %Y')`"
output:
  rmarkdown::html_vignette:
    keep_md: true
vignette: >
  %\VignetteIndexEntry{Local authentication}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(
    eval = TRUE, echo = TRUE,
    message = FALSE, warning = FALSE, error = FALSE,
    collapse = TRUE, comment = "#>",
    fig.width = 8, fig.height = 4, out.width = "100%", dpi = 300,
    cache = FALSE
)

library(tidyverse)
library(PhilipsHue)

reset_auth()
```



# Introduction

In order to control your Hue devices, you must first authenticate to your Hue
bridge. The PhilipsHue package only supports local authentication which means
that it can only be used when you are on the same network as the Hue bridge
you're trying to control. For local authentication to work, the PhilipsHue
packages requires the following environment variables to be set:

  - `PHILIPS_HUE_BRIDGE_IP`
  - `PHILIPS_HUE_BRIDGE_USERNAME` (In V2 of the Hue API, "username" is renamed
    to "application key" to emphasize that it is a key that should be kept
    secret.)

See the Philips Hue
[Getting Started](https://developers.meethue.com/develop/get-started-2/)
guide to learn more about authentication.



# Local authentication

Starting out, you can see that we have not set the necessary local
authentication environment variables.

```{r local_env_list}
Sys.getenv("PHILIPS_HUE_BRIDGE_IP")
Sys.getenv("PHILIPS_HUE_BRIDGE_USERNAME")
```

Let's begin by identifying the IP address of our local bridge. This can be found
in the settings of the Hue app, or we can use a lookup service as shown here.
(IP address masked for privacy.)

```{r local_ip}
bridge_ip <- httr::GET("https://discovery.meethue.com") |>
    httr::content() |>
    purrr::map_chr("internalipaddress")

gsub("\\d", "0", bridge_ip)
```

Now that we've found our bridge IP address, we can set the value as an
environment variable.

```{r local_ip_env}
Sys.setenv(PHILIPS_HUE_BRIDGE_IP = bridge_ip)
```

Next we need to create a new user -- there's a function for that!

```{r local_username_fail, error = TRUE}
create_user("test_user")
```

Oops! As you can see, in order to create a new user we need to press the button
on the Hue bridge first. Let's press the button and try again.

```{r include = FALSE}
message("Press bridge button now")
Sys.sleep(15)
```

```{r local_username}
local_user <- create_user("test_user")

purrr::map_lgl(local_user, grepl, pattern = "^.{2,}$")
```

Success! Now we can set the username environment variable, and we should have
local access to the bridge.

```{r local_username_env}
Sys.setenv(PHILIPS_HUE_BRIDGE_USERNAME = local_user$username)
```

With these variables set, PhilipsHue functions should take care of
authentication automatically. Here's a minimal example.

```{r local_eg}
get_lights() |> length()
get_light("1")$state

get_sensors() |> length()
get_sensor("1")$name
```



# `.Renviron` file

You can use an `.Renviron` file to set these environment variables automatically
when you start R. Here, we'll write these new credentials to a file that is used
during functional testing.

```{r save_local}
write_auth("tests/testthat/.Renviron", append = FALSE)
```
