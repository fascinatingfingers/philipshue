
# Copy *.md files to *.Rmd files
md_paths <- "vignettes-raw" |>
    fs::dir_ls(type = "file", recurse = TRUE, glob = "*.md")

md_paths_new <- fs::path("vignettes", fs::path_ext_set(fs::path_file(md_paths), "Rmd"))

fs::file_copy(md_paths, md_paths_new, overwrite = TRUE)

# Copy assets
md_files_paths <- "vignettes-raw" |>
    fs::dir_ls(type = "directory", recurse = TRUE, glob = "_files")

md_files_paths_new <- fs::path("vignettes", fs::path_file(md_files_paths))

if (any(fs::dir_exists(md_files_paths_new))) {
    fs::dir_delete(md_files_paths_new[fs::dir_exists(md_files_paths_new)])
}

fs::dir_copy(md_files_paths, md_files_paths_new, overwrite = TRUE)
