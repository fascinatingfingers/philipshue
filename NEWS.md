
<!-- copy/paste template
# Version 0.0.0
  - <Added|Changed|Deprecated|Removed|Fixed> ...
  - <Added|Changed|Deprecated|Removed|Fixed> ...
  - <Added|Changed|Deprecated|Removed|Fixed> ...
-->

# Version 1.0.0.9000
  - Added `PhilipsHue::clip_v2_endpoints` dataset describing all of the new V2
    API endpoints.

# Version 1.0.0
  - Added authentication helpers to authenticate to a Hue bridge on your local
    network
  - Added wrapper functions for most Hue API V1 endpoints
  - Added `configure_daylight_sensor()` to simplify setting your location
